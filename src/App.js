import React, { Component, useState } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    todosToAdd: [],
    clearSelected: false
  };

  handleInput = event =>
  {
    
    if(event.key === "Enter")
    {
      
      const newTodo = {"userId": 1,
      "id": Math.floor(Math.random() * 1000),
      "title": event.target.value,
      "completed": false}
      
      this.state.todosToAdd.push(newTodo)
      this.setState((state) => ({todosToAdd: state.todosToAdd}))
      event.target.value = ""
    }
    
  }
  
  handleClear = () =>
  {
    this.setState(() => ({clearSelected: true}))
    
  }
  
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" onKeyDown = {this.handleInput} placeholder="What needs to be done?" autofocus />
        </header>
        <TodoList todos={this.state.todos} 
        todosToAdd = {this.state.todosToAdd} 
        clearSelected = {this.state.clearSelected}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick ={this.handleClear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  state =
  {
    completed: this.props.completed,
    title: this.props.title
     
  }
  
  handleToggle = ()=> 
  {
    if(!this.state.completed)
    {
     this.setState(() => ({completed: true}))
     
    }
    else{
      this.setState(() => ({completed: false}))
      
    }
    
    
  }
  
  
  render() {
    return (
      <li className={this.state.completed? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" 
          onClick ={this.handleToggle} 
           onChange ={()=> this.props.imChecked(this.state)}
          checked={this.state.completed} />

          <label>{this.props.title}</label>
          <button className="destroy" onClick = {() => this.props.detleMe(this.props.title)} />
        </div>
      </li>
    );
  }
}

const TodoList = (props) =>
{
  
  
 const handleDelete =(value) =>
 {
   const newlistItems =listItems.filter((item) => item.title !== value)
   
    setListItems(newlistItems)
 }  

  const [listItems,setListItems] = useState(props.todos)

  let checkItems = []


  listItems.forEach((item) =>
  {
    if(item.completed)
    {
      checkItems.push(item.title)
    }
  })

  const handleCheckedItems = (value) =>
  {
    if(!value.completed)
    {
      checkItems.push(value.title)
    }
    else{
      const itemIndex =checkItems.indexOf(value.title)
      if(itemIndex >= 0)
      {
        checkItems.splice(itemIndex,1)
      }
      
    }
    
  }
  props.todosToAdd.forEach((item,index) => {
    listItems.push(item)
    props.todosToAdd.splice(index,1)
    
  })
  
  if(props.clearSelected)
  {
    
    // TODO delete selected items
    
    
  }
 
    return (
      <section className="main">
        <ul className="todo-list">
          {listItems.map((todo) => (
            <TodoItem title={todo.title} 
            completed={todo.completed} 
            detleMe ={handleDelete}
            imChecked = {handleCheckedItems}
            />
          ))}
        </ul>
      </section>
    );
}


export default App;




